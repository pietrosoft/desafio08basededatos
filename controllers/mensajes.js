const { sqlite3 } = require ('../db/config');
const knexFn = require ('knex');
const knex = knexFn(sqlite3)


module.exports.createTableMessages =  async function createTableMessages() {
    try {
        
        let exist = await knex.schema.hasTable('mensajes');
        //console.log(exist);
        if(!exist){
        await knex.schema.createTable('mensajes', table => {
            table.increments('id');
            table.string('text');
            table.string('user');
            table.timestamp('date', { useTz: true }).notNullable().defaultTo(knex.fn.now());
            console.log('TABLA MENSAJES CREADA CON EXITO')
        });
        }else{
            console.log('Tabla Mensajes Existe.')
        }
    } catch (error) {
        console.log('ERROR CREATE TABLE', error)
    }

};

module.exports.addMessages =   async function addMessages(mensaje) {
    console.log('Nuevo Mensaje:',mensaje)
    return knex('mensajes').insert(mensaje);
};

module.exports.listAllMessages =  async function listAllMessages() {
     //knex('mensajes').select();
     let mensajes = knex.select().from('mensajes')
    //console.log("SELECT" + mensajes);
    return mensajes 
}

module.exports.close =  async function close() {
    return knex.destroy();
}
