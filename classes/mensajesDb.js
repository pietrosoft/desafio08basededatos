
//import {createTableMessages,addMessages,listAllMessages} from './controllers/mensajes.js';
const { sqlite3 } = require ('../db/config.js');
const {createTableMessages,addMessages,listAllMessages} = require( '../controllers/mensajes.js');
//import {createTableMessages,addMessages,listAllMessages} from './controllers/mensajes.js';

class MensajesDb{

    constructor(){

        this.arr_mensajes=[];
        this.nextId=0;

        try{
            //this.arr_mensajes = this.loadMessages()
            //let file_content=fs.readFileSync('./mensajes.json','utf-8')
            //this.arr_mensajes= JSON.parse(file_content)
            //console.log(arr_products)
        
            }catch(err){
                console.log('No se encontraron mensajes :' + err)
            }

    }

    async initialize() {
        createTableMessages(sqlite3);
        this.arr_mensajes =  await listAllMessages()
        console.log( this.arr_mensajes);
     }
   
    /*static async create() {
        const o = new MensajesDb();
        await o.initialize();
        return o;
     }*/

    getNextId(){
        return ++this.nextId;
    }

    listAll(){
        if(this.arr_mensajes){
            return this.arr_mensajes;
        }
        else{
            return({info:'No hay mensajes.'});
        }
    }


    /*addMessage(user,text){       
        let msg={}
        msg.user=user;
        msg.text=text;
        msg.date=new Date().toLocaleString('ddMMyyy');
        msg.id=this.getNextId()
   
            console.log("guardando Mensaje: " + JSON.stringify(msg))
            this.arr_mensajes.push(msg);
        //fs.writeFileSync('./mensajes.json',JSON.stringify(this.arr_mensajes))    
        //mnjDB.push(msg)
        //await addMessages(msg)
        return(msg);
    }*/

    async addMessage(user,text){       
        let msg={}
        msg.user=user;
        msg.text=text;
        msg.date=new Date().toLocaleString('ddMMyyy');
        msg.id=this.getNextId()
   
            console.log("guardando Mensaje: " + JSON.stringify(msg))
            this.arr_mensajes.push(msg);
        //fs.writeFileSync('./mensajes.json',JSON.stringify(this.arr_mensajes))    
        //mnjDB.push(msg)
        await addMessages(msg)
        return(msg);
    }

    //TODO Guardar en archivo
    //TODO LEER EL ARCHIVO DE MENSAJES

}

module.exports = MensajesDb