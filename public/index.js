const socket = io();

socket.on('getConnection',(mensaje)=>{
    console.log(mensaje)
});

socket.on('getProductos',(liveProductos)=>{
    //console.log("Socket.on(getProductos)")
    updateProductos(liveProductos)
});

socket.on('getMensajes', function(data) { 
    console.log("RECIBIDO: "+ data);
    UpdateMensajes(data);
  });


/* BEGIN MANEJO DEL FORMULARIO */
    const form = document.querySelector('form');
    
    form.addEventListener('submit',e => {
    e.preventDefault();
    const data ={title:form[0].value, price:form[1].value,thumbnail:form[2].value};
    //console.log(data);

    fetch("/api/productos/guardar",
    {   
        headers:{'content-type' : 'application/json'},
        method: 'POST',
        body : JSON.stringify(data)
    })
    .then(function(response) {
      return response.text()
    }).then(function(text) {
        //console.log("Response:" + text);
        form.reset();

    });
});
/* END MANEJO DEL FORMULARIO */


/* BEGIN MANEJO DEL LISTA */
function updateProductos(liveProductos){
    //let template = document.getElementById('template_productos').innerHTML
    let template ='{{#if hasLiveProductos}}'+
    '   <div class="table-responsive">'+
    '   <table class="table table-dark">'+
    '       <tr> <th>id</th> <th>Nombre</th> <th>Precio</th> <th>Foto</th></tr>'+
    '       {{#each liveProductos}}'+
    '           <tr> <td>{{{this.id}}}</td> <td>{{this.title}}</td> <td>${{this.price}}</td> <td><img width="50" src={{this.thumbnail}} alt="not found"></td> </tr>'+
    '       {{/each}}'+
    '   </table>'+
    '</div>'+
    '{{else}}  '+
    '<h3 class="alert alert-warning">No se encontraron productos</h3>'+
    '{{/if}}';

    let renderProductos = Handlebars.compile(template)   
    if(liveProductos && liveProductos.length>0){
        //document.getElementById('tabla_productos').innerHTML=renderProductos({productosB:listProductos,hasProductosB:true});
        document.getElementById('tabla_productos').innerHTML=renderProductos({liveProductos:liveProductos,hasLiveProductos:true});
        //console.log(renderProductos({liveProductos:liveProductos,hasLiveProductos:true}));
        //console.log(liveProductos)
    }
    else{
        document.getElementById('tabla_productos').innerHTML=renderProductos({liveProductos:null,hasLiveProductos:false});
    }
    //renderProductos({productos:listProductos,hasProductos:(listProductos.length > 0)});
    //document.getElementById('tabla_productos').innerHTML=document.getElementById('template_productos').innerHTML
}
/* END MANEJO DEL LISTA */







