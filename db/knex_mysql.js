const opt_mysql=require('../config/db_mysql');
const knex_mysql = require('knex')(opt_mysql);

// exporto el objeto para usarlo en otros modulos
module.exports = knex_mysql;