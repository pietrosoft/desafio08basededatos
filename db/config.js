const mysql = {
    client: 'mysql',
    connection: {
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'productos'
    }
};

const sqlite3 = {
    client: 'sqlite3',
    connection: { filename: "./db/mensajes.sqlite" },
    useNullAsDefault: true
};

module.exports = {
    sqlite3,
    mysql

  }

