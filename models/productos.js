export default class Productos{  
    
    constructor(){
        this.arr_products=[];
        this.nextId=0;
    }

    getProductsCount(){
        if(this.arr_products)
            return (this.arr_products.length)
        else   
            return 0
    }

    getNextId(){
        return ++this.nextId;
    }

    listAll(){
        if(this.arr_products){
            return this.arr_products;
        }
        else{
            return({info:'No hay productos.'});
        }
    }

    listById(id){

        id = parseInt (id)
        let product =  this.arr_products.filter(it => it.id === id); 
        
        if(product[0])
            //res.json(product)
            return(product)
        else 
            return({error:'Producto no Encontrado.'})
    }

    addProduct(title,price,thumbnail){
            
        let prod={}
            prod.title=title
            prod.price=price
            prod.thumbnail=thumbnail
            prod.id=this.getNextId()
   
            console.log("guardando producto: " + JSON.stringify(prod))
            this.arr_products.push(prod);

        return(prod);
    }

    updateProduct(id,title,price,thumbnail){

        let idprod = parseInt (id)
        let flag_upd=false
        let prod = null 
        this.arr_products.forEach(it =>{
            if(it.id==idprod ){
                it.title=title;
                it.price=price;
                it.thumbnail=thumbnail;
                console.log("Id:"+idprod+" Acualizado")
                prod = it;
                flag_upd=true
            }
        })
    
       
        if(flag_upd)
            return({status:'OK',id:idprod, producto: prod})
        else 
            return({error:'Producto no Encontrado.'})

    }

    deleteProduct(id){
        let idprod = parseInt (id)
        let flag_delete=false 
        let producto_eliminado=null
        for (let i=0;i<this.arr_products.length;i++){
            if(this.arr_products[i].id==idprod ){
                console.log("Id:"+idprod+" eliminado")
                flag_delete=true
                producto_eliminado=this.arr_products[i];
                this.arr_products.splice(i,1)
            }
    
        }
        
           
        if(flag_delete)
            return({status:'Delete OK',id:idprod, producto: producto_eliminado})
        else 
            return({error:'Producto no Encontrado.'})
    }

}


