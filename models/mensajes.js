
const knex = require('../db/knex_sqlite3');
class Mensajes{

    constructor(){
        this.arr_mensajes=[];
        try{
            console.log("Cargando mensajes de la base de datos");
            this.loadAll()
            }catch(err){
                console.log('No se encontraron mensajes :' + err)
            }

    }

    listAll(){
        if(this.arr_mensajes){
            return this.arr_mensajes;
        }
        else{
            return({info:'No hay mensajes.'});
        }
    }

    addMessage(user,text){       
        let msg={}
        msg.user=user;
        msg.text=text;
        msg.date=new Date();
        //msg.id=this.getNextId() // el id es automatico.
        console.log("guardando Mensaje: " + JSON.stringify(msg))
        this.arr_mensajes.push(msg);
        //grabo el mensaje
        this.save(msg);
        return(msg);
    }

    async save(mensaje) {
        try {
            let resultado = await knex('mensajes').insert(mensaje);
            //console.log(resultado);
            return resultado;
        } catch (error) {
            throw error;
        }
    }

    async loadAll() {
        try {
            //let mensajes = await knex('mensajes').where(condicion);
            let rows = await knex.from('mensajes').select("*")
            rows.forEach(row => {
                let msg={}
                msg.user=row.user;
                msg.text=row.text;
                msg.date=row.date;
                msg.id=row.id
                this.arr_mensajes.push(msg);
            });
            //console.log(this.arr_mensajes)
        } catch (error) {
            throw error;
        }
    }

}

module.exports = Mensajes
